<?php

require_once 'vendor/autoload.php';

use DI\ContainerBuilder;

$containerBuilder = new ContainerBuilder;
$containerBuilder->addDefinitions('app/config.php');

$container = $containerBuilder->build();

if (!isset($argv[1])) {
	echo "please provide a sentence to translate. \n";
	exit(-1);
}

$sentence = $argv[1];

$sentenceTranslator = $container->get('PigLatin\SentenceTranslator');

echo "\n";
echo $sentenceTranslator->translate($sentence);
echo "\n";