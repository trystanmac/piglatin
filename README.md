# README #

### This repository provides a dedicated Pig Latin translator that can be plugged into any project ###

* Version 0.2

## About Pig Latin ##

Pig Latin is a language game in which words in English are altered. The objective is to conceal the meaning of the words from others not familiar with the rules.
The usual rules for changing English into Pig Latin are as follows:

 - In words that begin with consonant sounds, the initial consonant or consonant cluster is moved to the end of the word, and "ay" is added, as in the following examples: 

    * beast → east-bay
    * dough → ough-day
    * happy → appy-hay
    * question → estion-quay
    * star → ar-stay
    * three → ee-thray

 - In words that begin with vowel sounds or silent consonants, the syllable "ay" is added to the end of the word. In some dialects, to aid in pronunciation, an extra consonant is added to the beginning of the suffix; for instance, eagle could yield eagle'yay, eagle'way, or eagle'hay.

 - Transcription varies. A hyphen or apostrophe is sometimes used to facilitate translation back into English. Ayspray, for instance, is ambiguous, but ay-spray means "spray" whereas ays-pray means "prays."

## What is required ##

Please write an implementation of a Pig Latin translator that can translate strings of English text into Pig Latin as a PHP package.
It need not be complete enough to be executable, but should demonstrate your approach, skills and development style.
To be very specific; we are not looking for a nifty specific algorithm for making pig latin – we are looking at how you write well structured, maintainable, efficient OO code that would fit as part of an overall system.

### How do I get set up? ###

* Pull or download
* Run "composer install" in project base directory
* Run "vendor/bin/phpunit --verbose" in project base directory
* Tests should all pass!
* If you need to use the translator within an existing project using frameworks such as Zend, Symfony, Silex etc... integration instructions can be found at http://php-di.org

### Classes available in DI ###

* PigLatin\WordTranslator
* PigLatin\SentenceTranslator

### Example usage ###

See console.php

This can be run as follows: 

* $ php console.php "The quick brown fox jumps over the lazy dog trump three"

### To do ###

* Handle words beginning qu e.g quack should translate to ack-quay
* Handle case e.g. The should translate to E-thay
* Add logging