<?php

namespace PigLatin\Translator;

/**
 * Translator getter class for PigLatin based on word parameter
 *
 * @package    PigLatin
 * @author     Trystan MacDonald <trystan.macdonald@gmail.com>
 */

use PigLatin\Translator\Vowel;

/**
 * Class Type
 */
class Type
{
    protected $vowel;
    protected $consonant;

    /**
     * Constructor
     * 
     * @param \PigLatin\Translator\Vowel $vowel
     * @param \PigLatin\Translator\Consonant $consonant
     */
    public function __construct(
        Vowel $vowel,
        Consonant $consonant
    ) {
        $this->vowel = $vowel;
        $this->consonant = $consonant;
    }

    /**
     * get
     * 
     * @param type $word
     * @return Vowel|Consonant
     */
    public function get(string $word)
    {
        $word = strtolower($word);

        if ($this->wordBeginsWithVowel($word) or $this->wordBeginsWithSilentConsonent($word)) {
            return $this->vowel;
        }

        return $this->consonant;
    }

    protected function wordBeginsWithVowel($word)
    {
        return in_array($word{0}, ['a', 'e', 'i', 'o', 'u']);
    }

    protected function wordBeginsWithSilentConsonent($word)
    {
        $silentConsonent = substr($word, 0, 2);

        return in_array($silentConsonent, ['kn', 'gn', 'wr']);
    }
}
