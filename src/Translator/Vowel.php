<?php

namespace PigLatin\Translator;

/**
 * Concrete Translator class for PigLatin words beginning with a Vowel
 *
 * @package    PigLatin
 * @author     Trystan MacDonald <trystan.macdonald@gmail.com>
 */

use \PigLatin\TranslatorInterface;

/**
 * Class Vowel
 */
class Vowel implements TranslatorInterface
{
    /**
     * Translate
     * 
     * @param type $word
     * @return string
     */
    public function translate(string $word): string
    {
        return "{$word}-ay";
    }
}
