<?php

namespace PigLatin\Translator;

/**
 * Concrete Translator class for PigLatin words beginning with a Consonant
 *
 * @package    PigLatin
 * @author     Trystan MacDonald <trystan.macdonald@gmail.com>
 */

use \PigLatin\TranslatorInterface;

/**
 * Class Consonant
 */
class Consonant implements TranslatorInterface
{
    /**
     * Translate
     * 
     * @param type $word
     * @return string
     */
    public function translate(string $word): string
    {
        preg_match_all("/([aeiou])(.*)/i", $word, $matches);

        $body = $matches[0][0];

        $consonantSound = str_replace($body, "", $word);

        return "{$body}-{$consonantSound}ay";
    }
}
