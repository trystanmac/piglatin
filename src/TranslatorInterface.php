<?php

namespace PigLatin;

/**
 * Translator Interface for PigLatin words
 *
 * @package    PigLatin
 * @author     Trystan MacDonald <trystan.macdonald@gmail.com>
 */

interface TranslatorInterface
{
    /**
     * Translate
     * 
     * @param type $word
     * @return string
     */
    public function translate(string $word): string;
}
