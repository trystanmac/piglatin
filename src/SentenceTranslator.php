<?php

namespace PigLatin;

/**
 * Generic Sentence Translator class for PigLatin
 *
 * @package    PigLatin
 * @author     Trystan MacDonald <trystan.macdonald@gmail.com>
 */

use PigLatin\WordTranslator;
use PigLatin\TranslatorInterface;

/**
 * Class SentenceTranslator
 */
class SentenceTranslator implements TranslatorInterface
{
    protected $translator;

    /**
     * Constructor
     * 
     * @param \PigLatin\WordTranslator $translator
     * @return type
     */
    public function __construct(WordTranslator $translator)
    {
        $this->translator = $translator;
    }

    /**
     * Translate
     * 
     * @param type $word
     * @return string
     */
    public function translate(string $sentence): string
    {
        $translatedWords = [];

        foreach (explode(' ', $sentence) as $word) {
            $translatedWords[] = $this->translator->translate($word);
        }

        return implode(' ', $translatedWords);
    }
}
