<?php

namespace PigLatin;

/**
 * Generic WordTranslator class for PigLatin
 *
 * @package    PigLatin
 * @author     Trystan MacDonald <trystan.macdonald@gmail.com>
 */

use PigLatin\Translator\Type;

/**
 * Class WordTranslator
 */
class WordTranslator implements TranslatorInterface
{
    protected $type;

    /**
     * Constructor
     * 
     * @param \PigLatin\Translator\Type $type
     */
    public function __construct(Type $type)
    {
        $this->type = $type;
    }

    /**
     * Translate
     * 
     * @param type $word
     * @return string
     */
    public function translate(string $word): string
    {
        return $this
            ->type
            ->get($word)
            ->translate($word);
    }
}
