<?php

namespace PigLatinTests\Unit\Translator;

/**
 * Tests for Vowel class for PigLatin
 *
 * @package    PigLatin
 * @author     Trystan MacDonald <trystan.macdonald@gmail.com>
 */


use PHPUnit\Framework\TestCase;
use PigLatin\Translator\Vowel;
use PigLatin\TranslatorInterface;

/**
 * Class TypeTest
 */
class VowelTest extends TestCase
{
    protected $objectUnderTest;

    /**
     * setUp
     */
    public function setUp()
    {
        $this->objectUnderTest = new Vowel;
    }

    /**
     * testIsTranslatorInterface
     */
    public function testIsTranslatorInterface()
    {
        $this->assertInstanceOf(
            TranslatorInterface::class,
            $this->objectUnderTest
        );
    }

    /**
     * testTranslate_beast
     */
    public function testTranslate_beast()
    {
        $word = "eat";
        $translation = "eat-ay";

        $this->assertEquals(
            $translation,
            $this->objectUnderTest->translate($word)
        );
    }

    /**
     * testTranslate_omlet
     */
    public function testTranslate_omlet()
    {
        $word = "omelet";
        $translation = "omelet-ay";

        $this->assertEquals(
            $translation,
            $this->objectUnderTest->translate($word)
        );
    }

    /**
     * testTranslate_are
     */
    public function testTranslate_are()
    {
        $word = "are";
        $translation = "are-ay";

        $this->assertEquals(
            $translation,
            $this->objectUnderTest->translate($word)
        );
    }

    /**
     * testTranslate_upper
     */
    public function testTranslate_upper()
    {
        $word = "upper";
        $translation = "upper-ay";

        $this->assertEquals(
            $translation,
            $this->objectUnderTest->translate($word)
        );
    }
}
