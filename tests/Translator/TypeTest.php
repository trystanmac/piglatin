<?php

namespace PigLatinTests\Unit\Translator;

/**
 * Tests for Type class for PigLatin
 *
 * @package    PigLatin
 * @author     Trystan MacDonald <trystan.macdonald@gmail.com>
 */


use PHPUnit\Framework\TestCase;
use \PigLatin\Translator as Translator;

/**
 * Class TypeTest
 */
class TypeTest extends TestCase
{
    protected $objectUnderTest;
    protected $vowelMockTest;
    protected $consonentMockTest;

    /**
     * setUp
     */
    public function setUp()
    {
        $this->vowelMockTest = $this->createMock(Translator\Vowel::class);
        $this->consonentMockTest = $this->createMock(Translator\Consonant::class);

        $this->objectUnderTest = new Translator\Type(
            $this->vowelMockTest,
            $this->consonentMockTest
        );
    }

    /**
     * testGet_vowel_a_lower
     */
    public function testGet_vowel_a_lower()
    {
        $this->assertSame(
            $this->vowelMockTest,
            $this->objectUnderTest->get('anytime')
        );
    }

    /**
     * testGet_vowel_A_upper
     */
    public function testGet_vowel_A_upper()
    {
        $this->assertSame(
            $this->vowelMockTest,
            $this->objectUnderTest->get('Anytime')
        );
    }

    /**
     * testGet_vowel_e_lower
     */
    public function testGet_vowel_e_lower()
    {
        $this->assertSame(
            $this->vowelMockTest,
            $this->objectUnderTest->get('else')
        );
    }

    /**
     * testGet_vowel_E_upper
     */
    public function testGet_vowel_E_upper()
    {
        $this->assertSame(
            $this->vowelMockTest,
            $this->objectUnderTest->get('Else')
        );
    }

    /**
     * testGet_vowel_i_lower
     */
    public function testGet_vowel_i_lower()
    {
        $this->assertSame(
            $this->vowelMockTest,
            $this->objectUnderTest->get('indicate')
        );
    }

    /**
     * testGet_vowel_I_upper
     */
    public function testGet_vowel_I_upper()
    {
        $this->assertSame(
            $this->vowelMockTest,
            $this->objectUnderTest->get('Indicate')
        );
    }

    /**
     * testGet_vowel_o_lower
     */
    public function testGet_vowel_o_lower()
    {
        $this->assertSame(
            $this->vowelMockTest,
            $this->objectUnderTest->get('over')
        );
    }

    /**
     * testGet_vowel_O_upper
     */
    public function testGet_vowel_O_upper()
    {
        $this->assertSame(
            $this->vowelMockTest,
            $this->objectUnderTest->get('Over')
        );
    }

    /**
     * testGet_vowel_u_lower
     */
    public function testGet_vowel_u_lower()
    {
        $this->assertSame(
            $this->vowelMockTest,
            $this->objectUnderTest->get('upper')
        );
    }

    /**
     * testGet_vowel_U_upper
     */
    public function testGet_vowel_U_upper()
    {
        $this->assertSame(
            $this->vowelMockTest,
            $this->objectUnderTest->get('Upper')
        );
    }

    /**
     * testGet_silent_consonent_kn_lower
     */
    public function testGet_silent_consonent_kn_lower()
    {
        $this->assertSame(
            $this->vowelMockTest,
            $this->objectUnderTest->get('knee')
        );
    }

    /**
     * testGet_silent_consonent_kn_upper
     */
    public function testGet_silent_consonent_kn_upper()
    {
        $this->assertSame(
            $this->vowelMockTest,
            $this->objectUnderTest->get('KNee')
        );
    }

    /**
     * testGet_silent_consonent_gn_lower
     */
    public function testGet_silent_consonent_gn_lower()
    {
        $this->assertSame(
            $this->vowelMockTest,
            $this->objectUnderTest->get('gnomes')
        );
    }

    /**
     * testGet_silent_consonent_gn_upper
     */
    public function testGet_silent_consonent_gn_upper()
    {
        $this->assertSame(
            $this->vowelMockTest,
            $this->objectUnderTest->get('GNomes')
        );
    }

    /**
     * testGet_silent_consonent_wr_lower
     */
    public function testGet_silent_consonent_wr_lower()
    {
        $this->assertSame(
            $this->vowelMockTest,
            $this->objectUnderTest->get('write')
        );
    }

    /**
     * testGet_silent_consonent_wr_upper
     */
    public function testGet_silent_consonent_wr_upper()
    {
        $this->assertSame(
            $this->vowelMockTest,
            $this->objectUnderTest->get('write')
        );
    }

    /**
     * testGet_vowel_consonent
     */
    public function testGet_vowel_consonent()
    {
        $this->assertSame(
            $this->consonentMockTest,
            $this->objectUnderTest->get('picnic')
        );
    }
}
