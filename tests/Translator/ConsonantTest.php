<?php

namespace PigLatinTests\Unit\Translator;

/**
 * Tests for Consonant class for PigLatin
 *
 * @package    PigLatin
 * @author     Trystan MacDonald <trystan.macdonald@gmail.com>
 */

use PHPUnit\Framework\TestCase;

/**
 * Class WordTranslator
 */
class ConsonantTest extends TestCase
{
    protected $objectUnderTest;

    /**
     * Setup
     */
    public function setUp()
    {
        $this->objectUnderTest = new \PigLatin\Translator\Consonant;
    }

    /**
     * testIsTranslatorInterface
     */
    public function testIsTranslatorInterface()
    {
        $this->assertInstanceOf(
            \PigLatin\TranslatorInterface::class,
            $this->objectUnderTest
        );
    }

    /**
     * testTranslate_beast
     */
    public function testTranslate_beast()
    {
        $word = 'beast';
        $translation = 'east-bay';


        $this->assertEquals(
            $translation,
            $this->objectUnderTest->translate($word)
        );
    }

    /**
     * testTranslate_dough
     */
    public function testTranslate_dough()
    {
        $word = 'dough';
        $translation = 'ough-day';


        $this->assertEquals(
            $translation,
            $this->objectUnderTest->translate($word)
        );
    }

    /**
     * testTranslate_happy
     */
    public function testTranslate_happy()
    {
        $word = 'happy';
        $translation = 'appy-hay';


        $this->assertEquals(
            $translation,
            $this->objectUnderTest->translate($word)
        );
    }

    /**
     * testTranslate_question
     */
    public function testTranslate_question()
    {
        $this->markTestIncomplete('We need to recognise words begining qu - question.');

        $word = 'question';
        $translation = 'estion-quay';


        $this->assertEquals(
            $translation,
            $this->objectUnderTest->translate($word)
        );
    }

    /**
     * testTranslate_star
     */
    public function testTranslate_star()
    {
        $word = 'star';
        $translation = 'ar-stay';


        $this->assertEquals(
            $translation,
            $this->objectUnderTest->translate($word)
        );
    }

    /**
     * testTranslate_three
     */
    public function testTranslate_three()
    {
        $word = 'three';
        $translation = 'ee-thray';


        $this->assertEquals(
            $translation,
            $this->objectUnderTest->translate($word)
        );
    }
}
