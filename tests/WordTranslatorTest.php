<?php

namespace PigLatinTests\Unit;

/**
 * Tests for WordTranslator class for PigLatin
 *
 * @package    PigLatin
 * @author     Trystan MacDonald <trystan.macdonald@gmail.com>
 */

use PHPUnit\Framework\TestCase;
use DI\ContainerBuilder;
use PigLatin\Translator\Type;
use PigLatin\WordTranslator;
use PigLatin\TranslatorInterface;

/**
 * Class WordTranslatorTest
 */
class WordTranslatorTest extends TestCase
{
    protected $objectUnderTest;
    protected $translatorTypeMock;

    /**
     * setUp
     */
    public function setUp()
    {
        $this->translatorTypeMock = $this->createMock(Type::class);

        $this->objectUnderTest = new WordTranslator($this->translatorTypeMock);
    }

    /**
     * testIsTranslatorInterface
     */
    public function testIsTranslatorInterface()
    {
        $this->assertInstanceOf(
            TranslatorInterface::class,
            $this->objectUnderTest
        );
    }

    /**
     * testTranslate
     */
    public function testTranslate()
    {
        $word = 'some_word';

        $translatorMock = $this->createMock(TranslatorInterface::class);

        $this
            ->translatorTypeMock
            ->expects($this->once())
            ->method('get')
            ->with($word)
            ->will($this->returnValue($translatorMock));

        $returnWord = 'some other word';
        $translatorMock
            ->expects($this->once())
            ->method('translate')
            ->with($word)
            ->will($this->returnValue($returnWord));

        $this->assertEquals(
            $returnWord,
            $this->objectUnderTest->translate($word)
        );
    }

    /**
     * testTranslate_FromDi
     */
    public function testTranslate_FromDi()
    {
        $translator = $this
            ->getContainer()
            ->get(WordTranslator::class);

        $this->assertInstanceOf(
            WordTranslator::class,
            $translator
        );
    }

    protected function getContainer()
    {
        $containerBuilder = new ContainerBuilder;
        $containerBuilder->addDefinitions(__DIR__ . '/../app/config.php');

        return $containerBuilder->build();
    }
}
