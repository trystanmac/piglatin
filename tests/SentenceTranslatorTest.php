<?php

namespace PigLatinTests\Unit;

/**
 * Tests for SentenceTranslator class for PigLatin
 *
 * @package    PigLatin
 * @author     Trystan MacDonald <trystan.macdonald@gmail.com>
 */

use PHPUnit\Framework\TestCase;
use DI\ContainerBuilder;
use PigLatin\WordTranslator;
use PigLatin\SentenceTranslator;
use PigLatin\TranslatorInterface;

/**
 * Class WordTranslatorTest
 */
class SentenceTranslatorTest extends TestCase
{
    protected $objectUnderTest;
    protected $translatorMock;

    /**
     * setUp
     */
    public function setUp()
    {
        $this->translatorMock = $this->createMock(WordTranslator::class);

        $this->objectUnderTest = new SentenceTranslator($this->translatorMock);
    }

    /**
     * testIsTranslatorInterface
     */
    public function testIsTranslatorInterface()
    {
        $this->assertInstanceOf(
            TranslatorInterface::class,
            $this->objectUnderTest
        );
    }

    /**
     * testTranslate
     */
    public function testTranslate()
    {
        $sentence = "The quick brown fox jumps over the lazy dog";

        $map = [
            ['The', 'e-Thay'],
            ['quick', 'ick-quay'],
            ['brown', 'br-own'],
            ['fox', 'ox-fay'],
            ['jumps', 'upmps-jay'],
            ['over', 'over-ay'],
            ['the', 'e-thay'],
            ['lazy', 'azy-lay'],
            ['dog', 'og-day'],
        ];

        $this
            ->translatorMock
            ->method('translate')
            ->will($this->returnValueMap($map));

        $expectedSentence = "e-Thay ick-quay br-own ox-fay upmps-jay over-ay e-thay azy-lay og-day";

        $this->assertEquals(
            $expectedSentence,
            $this->objectUnderTest->translate($sentence)
        );
    }

    /**
     * testTranslate_FromDi
     */
    public function testTranslate_FromDi()
    {
        $sentenceTranslator = $this
            ->getContainer()
            ->get(SentenceTranslator::class);

        $this->assertInstanceOf(
            SentenceTranslator::class,
            $sentenceTranslator
        );
    }

    protected function getContainer()
    {
        $containerBuilder = new ContainerBuilder;
        $containerBuilder->addDefinitions(__DIR__ . '/../app/config.php');

        return $containerBuilder->build();
    }
}
