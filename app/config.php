<?php
use function DI\object;

/**
 * Config to provide a fully setup PigLatin\Translator in the DI
 *
 * @package    PigLatin
 * @author     Trystan MacDonald <trystan.macdonald@gmail.com>
 */

use PigLatin\WordTranslator;
use PigLatin\SentenceTranslator;

return [
    WordTranslator::class => object(WordTranslator::class),
    SentenceTranslator::class => object(SentenceTranslator::class),
];